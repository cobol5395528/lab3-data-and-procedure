       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA5.
       AUTHOR. PHADOL WONGSIRI.
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  GRADE-DATA  PIC   X(90) VALUE "63160009Phadol Wongsiri 886243
      -    "593A 886244593A 886245593A 886342593A 886344593B+886462593B+
      -    "".
       01  GRADE REDEFINES GRADE-DATA.
           03 STU-ID         PIC   9(8).
           03 STU-NAME       PIC   X(16).
           03 SUB1.
              05 SUB-CODE1      PIC   9(8).
              05 SUB-UNIT1      PIC   9.
              05 SUB-GRADE1     PIC   X(2).
           03 SUB2.
              05 SUB-CODE2      PIC   9(8).
              05 SUB-UNIT2      PIC   9.
              05 SUB-GRADE2     PIC   X(2).
           03 SUB3.
              05 SUB-CODE3      PIC   9(8).
              05 SUB-UNIT3      PIC   9.
              05 SUB-GRADE3     PIC   X(2).
           03 SUB4.
              05 SUB-CODE4      PIC   9(8).
              05 SUB-UNIT4      PIC   9.
              05 SUB-GRADE4     PIC   X(2).
           03 SUB5.
              05 SUB-CODE5      PIC   9(8).
              05 SUB-UNIT5      PIC   9.
              05 SUB-GRADE5     PIC   X(2).
           03 SUB6.
              05 SUB-CODE6      PIC   9(8).
              05 SUB-UNIT6      PIC   9.
              05 SUB-GRADE6     PIC   X(2).
       66  STUDENT-ID  RENAMES STU-ID.
       66  STUDENT-INFO   RENAMES STU-ID THRU SUB-UNIT1.
       01  STUCODE  REDEFINES GRADE-DATA.
           05 STU-YEAR       PIC   9(2).
           05 FILLER         PIC   X(6).
           05 STU-SHORT-NAME PIC   X(3).
       PROCEDURE DIVISION.
       Begin.
           MOVE GRADE-DATA TO GRADE
           DISPLAY GRADE

           DISPLAY  "SUBJECT 1"
           DISPLAY  "SUBJECT ID: " SUB-CODE1
           DISPLAY  "SUBJECT UNIT: " SUB-UNIT1
           DISPLAY  "SUBJECT GRADE: " SUB-GRADE1

           DISPLAY  "SUBJECT 2"
           DISPLAY  "SUBJECT ID: " SUB-CODE2
           DISPLAY  "SUBJECT UNIT: " SUB-UNIT2
           DISPLAY  "SUBJECT GRADE: " SUB-GRADE2

           DISPLAY  "SUBJECT 3"
           DISPLAY  "SUBJECT ID: " SUB-CODE3
           DISPLAY  "SUBJECT UNIT: " SUB-UNIT3
           DISPLAY  "SUBJECT GRADE: " SUB-GRADE3

           DISPLAY  "SUBJECT 4"
           DISPLAY  "SUBJECT ID: " SUB-CODE4
           DISPLAY  "SUBJECT UNIT: " SUB-UNIT4
           DISPLAY  "SUBJECT GRADE: " SUB-GRADE4

           DISPLAY  "SUBJECT 5"
           DISPLAY  "SUBJECT ID: " SUB-CODE5
           DISPLAY  "SUBJECT UNIT: " SUB-UNIT5
           DISPLAY  "SUBJECT GRADE: " SUB-GRADE5

           DISPLAY  "SUBJECT 6"
           DISPLAY  "SUBJECT ID: " SUB-CODE6
           DISPLAY  "SUBJECT UNIT: " SUB-UNIT6
           DISPLAY  "SUBJECT GRADE: " SUB-GRADE6

           DISPLAY SUB1
           DISPLAY STUDENT-ID
           DISPLAY STUDENT-INFO 
           DISPLAY STU-YEAR STU-SHORT-NAME 
           .